<?php
session_start();

define('PASSWORD_REQUIRED', true);
define('URL', 'http://'.$_SERVER['HTTP_HOST'] . '/');
define('ID_LENGTH', 6);
define('PASSWORD', 'Passw0rd!');
define('COLOR', '#005D5D');
define('FILTER', [
    'pornhub'
]);

function testId()
{
	$id = ltrim($_SERVER["REQUEST_URI"], "/");
	return $id !== "index.php" && $id !== "" && $id != "styles.css" && $id != "favicon.ico"; 
}
function checkFilters($link)
{
    $domain = parse_url($link, PHP_URL_HOST);
    foreach(FILTER as $filter) {
        $filter = '/(.*)'.preg_quote($filter).'(.*)/mi';
        if (preg_replace($filter, '', $link) !== $link) {
            return false;
        }
    }
    return true;
}
function adjustBrightness($hex, $steps)
{
    $steps = max(-255, min(255, $steps));

    $hex = str_replace('#', '', $hex);
    if (strlen($hex) == 3) {
        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
    }

    $color_parts = str_split($hex, 2);
    $return = '#';

    foreach ($color_parts as $color) {
        $color   = hexdec($color);
        $color   = max(0,min(255,$color + $steps));
        $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT);
    }

    return $return;
}
function generateID($length) 
{
    $str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    return substr(str_shuffle($str), 0, $length); 
}
function getID()
{
    $id = generateID(ID_LENGTH);
    while(file_exists('./links/'.$id.'.txt')) {
        $id = generateID(ID_LENGTH);
    }
    return $id;
}
function checkIfExists($link)
{
	if (!file_exists('./links')) {
		mkdir('./links');
	}
    foreach(scandir('./links') as $file) {
        $l = trim(file_get_contents('./links/'.$file));
        if (strtolower($l) === strtolower($link)) {
            return $file;
        }
    }
    return null;
}
function normalize($link)
{
    $url = parse_url($link);
    if (!isset($url["scheme"])) {
        $url["scheme"] = 'http';
    }
    
    $url["scheme"] = strtolower($url["scheme"]);
    $url["host"] = strtolower($url["host"]);
    
    $newLink = $url["scheme"] . '://' . $url["host"] . '/';
    
    if (isset($url["port"])) {
        $newLink .= ':' . $url["port"];
    }
    if (isset($url["path"])) {
        $newLink .= ltrim($url["path"], '/');
    }
    if (isset($url["query"])) {
        $newLink .= '?' . $url["query"];
    }
    if (isset($url["fragment"])) {
        $newLink .= '#' . $url["fragment"];
    }
    
    return $newLink;
}

if (isset($_GET["id"]) || testId())
{
	$dd = $_GET["id"];
	if (!isset($_GET["id"]))
	{
		$dd = ltrim($_SERVER["REQUEST_URI"], "/");
	}
	if (!file_exists('./links')) {
		mkdir('./links');
	}
	$file = "./links/".$dd.".txt";
	if (file_exists($file))
	{
		$link = trim(file_get_contents($file));
		
		if (!checkFilters($link)) {
			?>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>Link Shortener</title>
			<link rel="icon" type="image/x-icon" href="/favicon.ico" />
			<link rel="stylesheet" href="/styles.css" />
			<style>
			body,
			#message button[type="submit"] {
				background: <?php echo COLOR; ?>;
			}
			#message button[type="submit"]:hover {
				background: <?php echo adjustBrightness(COLOR, -20); ?>;
			}
			a {
				font-weight: bold;
				color: <?php echo COLOR; ?> !important;
			}
			pre {
				font-size: 13px;
				white-space: pre-wrap;
				word-wrap: break-word;
				-moz-control-character-visibility: visible;
				line-height: 15px;
				color: #000;
			}
			.dangerous {
				font-weight: bold;
				color: <?php echo COLOR; ?>;
			}
			</style>
			<div class="container">
				<div id="message">
					<pre>This link redirects to <span class="dangerous"><?php echo $link; ?></span>, which has been marked as a potentially dangerous link, click <a href="<?php echo $link; ?>">here</a> if you still want to visit it.</pre>
				</div>
			</div>
			<?php
		} else {
			header('Location: '.$link);
		}
		exit;
	}
	else
	{
		header("Content-type: text/plain");
		echo 'Invalid link.';
		exit;
	}
}

$id = null;

if (isset($_POST["link"]))
{
	$link = normalize(trim($_POST["link"]));
	if (empty($link) || !filter_var($link, FILTER_VALIDATE_URL)) {
		$id = false;
	} else {
        if (PASSWORD_REQUIRED && $_POST['password'] !== PASSWORD) {
            $id = false;
        } else {
            $exists = checkIfExists($link);
            if ($exists) {
                $id = str_replace('.txt', '', basename($exists));
            } else {
                $id = getID();
                $file = "./links/".$id.".txt";
                file_put_contents($file, $_POST["link"]);
            }
        }
    }
        
    $_SESSION['last_id'] = $id;
    header('Location: /');
    exit;
}

if (isset($_SESSION['last_id'])) {
    $id = $_SESSION['last_id'];
    session_destroy();
}

?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Link Shortener</title>
<link rel="icon" type="image/x-icon" href="/favicon.ico" />
<link rel="stylesheet" href="/styles.css" />
<style>
body,
#message button[type="submit"] {
    background: <?php echo COLOR; ?>;
}
#message button[type="submit"]:hover {
    background: <?php echo adjustBrightness(COLOR, -20); ?>;
}
a {
	font-weight: bold;
    color: <?php echo COLOR; ?> !important;
}
</style>
<div class="container">
    <form id="message" action="" method="post">
        <h3>Link Shortener</h3>
		<?php
		if (!$id) {
		?>
        <fieldset>
            <input type="url" placeholder="Enter Link here..." name="link" required />
        </fieldset>
        <?php echo PASSWORD_REQUIRED ? '<fieldset><input type="password" placeholder="Password" name="password" required /></fieldset>' : ''; ?>
        <?php echo $id === false ? '<fieldset><p style="color: red">Invalid Link'.(PASSWORD_REQUIRED ? ' or password' : '').'</p></fieldset>' : ''; ?>
        <fieldset>
            <button name="submit" type="submit" id="contact-submit">Shorten Link</button>
        </fieldset>
		<?php
		} else {
		?>
		<h4>Shortened link: <a id="link" href="<?php echo URL . $id; ?>"><?php echo URL . $id; ?></a>.<br><span id="msg">Click to copy to clipboard.</span></h4>
		<script>
		function copied() {
			document.getElementById("msg").innerHTML = "Copied to Clipboard!";
		}
		function error(err) {
			document.getElementById("msg").innerHTML = "Failed to copy to Clipboard!";
		}
		function fallbackCopyTextToClipboard(text) {
			var textArea = document.createElement("textarea");
			textArea.value = text;
			document.body.appendChild(textArea);
			textArea.focus();
			textArea.select();

			try {
				var successful = document.execCommand('copy');
				if (!successful) {
					throw "Error";
				}
				copied();
			} catch (err) {
				error(err);
			}

			document.body.removeChild(textArea);
		}

		function copyTextToClipboard(text) {
			if (!navigator.clipboard) {
				fallbackCopyTextToClipboard(text);
				return;
			}
			navigator.clipboard.writeText(text).then(function() {
				copied();
			}, function(err) {
				error(err);
			});
		}
		document.getElementById("link").onclick = function(e) {
			e.preventDefault();
			var copyText = document.getElementById("link").innerText;

			copyTextToClipboard(copyText);
		};
		</script>
		<?php
		}
		?>
    </form>
</div>